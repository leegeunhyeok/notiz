# Notiz

<div align="center">
  <img src="./sample.png">
</div>

#### Build Setup

``` bash
# 의존성 모듈 설치
npm install

# serve with hot reload at localhost:9080
npm run dev

# Electron 앱 빌드
npm run build

# unit & end-to-end 테스트
npm test


# 모든 js, vue 파일 lint 체크
npm run lint
```

