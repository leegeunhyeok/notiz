import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main-list',
      component: require('@/components/NotizMain').default
    },
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/setting',
      name: 'setting',
      component: require('@/components/NotizSetting').default
    },
    {
      path: '/edit',
      name: 'edit',
      component: require('@/components/NotizEdit').default
    }
  ]
})
