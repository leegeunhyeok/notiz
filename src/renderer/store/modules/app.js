const state = {
  setting: {},
  deletelist: [],
  note: ''
}

const mutations = {
  SET_SETTING (state, setting) {
    state.setting = setting
  },
  SET_COLOR (state, color) {
    state.setting.color = color
  },
  SET_NOTE (state, note) {
    state.note = note
  },
  RESET_DEL_LIST (state) {
    state.deletelist = []
  }
}

const actions = {}

export default {
  state,
  mutations,
  actions
}
