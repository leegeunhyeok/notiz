export default {
  'red': {
    name: '빨강',
    darkprimary: '#aa2e25',
    primary: '#f44336',
    accent: '#ff1744'
  },
  'pink': {
    name: '분홍',
    darkprimary: '#a31545',
    primary: '#e91e63',
    accent: '#f50057'
  },
  'purple': {
    name: '보라',
    darkprimary: '#6d1b7b',
    primary: '#9c27b0',
    accent: '#d500f9'
  },
  'deep-purple': {
    name: '진한 보라',
    darkprimary: '#482880',
    primary: '#673ab7',
    accent: '#651fff'
  },
  'indigo': {
    name: '남색',
    darkprimary: '#2c387e',
    primary: '#3f51b5',
    accent: '#3d5afe'
  },
  'blue': {
    name: '파랑',
    darkprimary: '#1769aa',
    primary: '#2196f3',
    accent: '#2979ff'
  },
  'light-blue': {
    name: '밝은 파랑',
    darkprimary: '#0276aa',
    primary: '#03a9f4',
    accent: '#00b0ff'
  },
  'cyan': {
    name: '청록',
    darkprimary: '#008394',
    primary: '#00bcd4',
    accent: '#00e5ff'
  },
  'teal': {
    name: '물오리',
    darkprimary: '#00695f',
    primary: '#009688',
    accent: '#1de9b6'
  },
  'green': {
    name: '초록',
    darkprimary: '#357a38',
    primary: '#4caf50',
    accent: '#00e676'
  },
  'light-green': {
    name: '밝은 초록',
    darkprimary: '#618833',
    primary: '#8bc34a',
    accent: '#76ff03'
  },
  'lime': {
    name: '라임',
    darkprimary: '#8f9a27',
    primary: '#cddc39',
    accent: '#c6ff00'
  },
  'yellow': {
    name: '노랑',
    darkprimary: '#b2a429',
    primary: '#ffeb3b',
    accent: '#ffea00'
  },
  'amber': {
    name: '등색',
    darkprimary: '#b28704',
    primary: '#ffc107',
    accent: '#ffc400'
  },
  'orange': {
    name: '주황',
    darkprimary: '#b26a00',
    primary: '#ff9800',
    accent: '#ffc400'
  },
  'deep-orange': {
    name: '진한 주황',
    darkprimary: '#b23c17',
    primary: '#ff5722',
    accent: '#ff3d00'
  }
}
